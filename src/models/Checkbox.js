import Component from "./Component";

export default class Checkbox extends Component {
  constructor() {
    super();
    this.type = "checkbox";
  }
}
