import Component from "./Component";

export default class TextArea extends Component {
  constructor() {
    super();
    this.type = "textarea";
  }
}
