export default class Component {
  constructor() {
    this.id = "";
    this.top = 0;
    this.left = 0;
    this.src = "";
    this.value = "";
    this.title = "";
    this.styles = {};
    this.tabIndex = 0;
    this.width = "auto";
    this.height = "auto";
  }
}
