import Component from "./Component";

export default class Button extends Component {
  constructor() {
    super();
    this.type = "button";
    this.value = "button";
  }
}
