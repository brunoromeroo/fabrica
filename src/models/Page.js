export default class Page {
  constructor() {
    this.title = "";
    this.video = "";
    this.items = [];
    this.done = false;
    this.itemsBlind = [];
    this.defaultImage = "";
  }
}
