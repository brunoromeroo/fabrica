export default [
  {
    done: false,
    video: "",
    defaultImage: "./images_conteudo/iconeIntroducao.png",
    title:
      "Nesta aula, abordaremos o conceito de Estruturas de Decisão em algoritmos. Também iremos entender o que é uma Estrutura de Decisão SE, SE/SENÃO, SELECIONE/CASO e Estruturas de Decisão Aninhada.",
    items: [],
    itemsBlind: []
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      "Simulador 01: Selecione “Verdadeiro” ou “Falso” para cada uma das caixas a seguir.",
    items: [
      {
        title: 'Selecione corretamente, depois, pressione "Validar".',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "4 > 4?",
                value: "4 > 4 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "294px",
                height: "63px",
                left: "55px",
                top: "110px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "105px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "'ARARA' == 'ARARA' ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "313px",
                height: "59px",
                left: "50px",
                top: "210px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "56 == 56 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "300px",
                height: "61px",
                left: "55px",
                top: "315px"
              },
              {
                id: "label3",
                type: "label",
                tabIndex: "",
                title: "",
                value: "8 < 7 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "300px",
                height: "55px",
                left: "55px",
                top: "420px"
              },
              {
                id: "label4",
                type: "label",
                tabIndex: "",
                title: "",
                value: "'CASA' != 'ASA' ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "301.969px",
                height: "58px",
                left: "54.984375px",
                top: "515px"
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "205px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "select2",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "305px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "select3",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "273px",
                height: "50px",
                left: "385px",
                top: "405px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "select4",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "505px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Validar.",
                styles: { fontSize: "22px" },
                src: "",
                width: "295px",
                height: "50px",
                left: "235px",
                top: "610px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r2",
                        select1: "r1",
                        select2: "r1",
                        select3: "r2",
                        select4: "r1"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title: 'Selecione corretamente, depois, pressione "Validar".',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "label0",
                type: "label",
                tabIndex: -1,
                title: "4 > 4?",
                value: "4 > 4 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "294px",
                height: "63px",
                left: "55px",
                top: "110px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "4 maior que 4? Escolha uma opção.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "105px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "label1",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "'ARARA' == 'ARARA' ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "313px",
                height: "59px",
                left: "50px",
                top: "210px"
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "'ARARA' == 'ARARA' ? Escolha uma opção.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "205px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "label2",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "56 == 56 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "300px",
                height: "61px",
                left: "55px",
                top: "315px"
              },
              {
                id: "select2",
                type: "select",
                tabIndex: "",
                title: "56 == 56 ? Escolha uma opção.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "305px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "label3",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "8 < 7 ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "300px",
                height: "55px",
                left: "55px",
                top: "420px"
              },
              {
                id: "select3",
                type: "select",
                tabIndex: "",
                title: "8 menor que 7 ? Escolha uma opção.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "273px",
                height: "50px",
                left: "385px",
                top: "405px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "label4",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "'CASA' != 'ASA' ?",
                styles: { fontSize: "30px" },
                src: "",
                width: "301.969px",
                height: "58px",
                left: "54.984375px",
                top: "515px"
              },
              {
                id: "select4",
                type: "select",
                tabIndex: "",
                title: "'CASA' diferente de 'ASA' ?",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "274px",
                height: "50px",
                left: "385px",
                top: "505px",
                values: {
                  r1: "Verdadeiro.",
                  r2: "Falso."
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Validar.",
                value: "Validar.",
                styles: { fontSize: "22px" },
                src: "",
                width: "295px",
                height: "50px",
                left: "235px",
                top: "610px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r2",
                        select1: "r1",
                        select2: "r1",
                        select3: "r2",
                        select4: "r1"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      "Simulador 02: Complete o algoritmo, na caixa ao lado, de forma que as respostas sejam: A=20, B=20 e C= 40.",
    items: [
      {
        title: 'Complete o algoritmo, depois, pressione, "Executar".',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "text0",
                type: "textarea",
                tabIndex: "",
                title: "",
                value:
                  "Variáveis \na, b, c : inteiro \nInício \n\ta= 12\n\tb=\n\ta = a + 3\nse a < 16 então\n\ta += 5\n\tb += 5\nfim_se\n\tc = a + b\nse b == 15 então \n\tb += 15 \nfim_se\nFIM",
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "25px",
                  backgroundColor: "transparent"
                },
                readOnly: true,
                src: "",
                width: "746px",
                height: "555px",
                left: "15px",
                top: "15px"
              },
              {
                id: "cx_text0",
                type: "input",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "41px",
                height: "25px",
                left: "165px",
                top: "135px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "220px",
                height: "50px",
                left: "500px",
                top: "580px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        cx_text0: "15"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title: 'Complete o algoritmo, depois, pressione, "Executar".',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "text0",
                type: "textarea",
                tabIndex: "",
                title: "",
                value:
                  "Variáveis \na, b, c : inteiro \nInício \n\ta= 12\n\tb=\n\ta = a + 3\nse a < 16 então\n\ta += 5\n\tb += 5\nfim_se\n\tc = a + b\nse b == 15 então \n\tb += 15 \nfim_se\nFIM",
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "25px",
                  backgroundColor: "transparent"
                },
                readOnly: true,
                src: "",
                width: "746px",
                height: "555px",
                left: "15px",
                top: "15px"
              },
              {
                id: "cx_text0",
                type: "input",
                tabIndex: "",
                title: "Digite o valor correto.",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "41px",
                height: "25px",
                left: "165px",
                top: "135px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Executar",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "220px",
                height: "50px",
                left: "500px",
                top: "580px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        cx_text0: "15"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      "Simulador 03: Analise o algoritmo a seguir e escolha a opção que o completa.",
    items: [
      {
        title:
          'Marque a variável “a” para que guarde o valor 10 ao fim do algoritmo, depois, pressione, "Executar"',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Estrutura",
                styles: { fontSize: "30px" },
                src: "",
                width: "205px",
                height: "50px",
                left: "25px",
                top: "70px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variável  :",
                styles: { fontSize: "30px" },
                src: "",
                width: "224px",
                height: "50px",
                left: "55px",
                top: "135px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a : inteiro",
                styles: { fontSize: "30px" },
                src: "",
                width: "218px",
                height: "50px",
                left: "105px",
                top: "190px"
              },
              {
                id: "label3",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a = 10",
                styles: { fontSize: "30px" },
                src: "",
                width: "217px",
                height: "57px",
                left: "105px",
                top: "250px"
              },
              {
                id: "label4",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Inicio",
                styles: { fontSize: "30px" },
                src: "",
                width: "219px",
                height: "57px",
                left: "105px",
                top: "320px"
              },
              {
                id: "label5",
                type: "label",
                tabIndex: "",
                title: "",
                value: "x = 10",
                styles: { fontSize: "30px" },
                src: "",
                width: "193px",
                height: "52px",
                left: "145px",
                top: "385px"
              },
              {
                id: "label6",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a + = 5",
                styles: { fontSize: "30px" },
                src: "",
                width: "185px",
                height: "58px",
                left: "165px",
                top: "505px"
              },
              {
                id: "label7",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim",
                styles: { fontSize: "30px" },
                src: "",
                width: "238px",
                height: "65px",
                left: "50px",
                top: "580px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "23px" },
                src: "",
                width: "365px",
                height: "53px",
                left: "105px",
                top: "445px",
                values: {
                  r1: "Se “a” <= 10, então, a=2, fim_se.",
                  r2: "Se a==10, então, a<=5, fim_se.",
                  r3: "Se a>10, então, a<=5, fim_se."
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "253px",
                height: "50px",
                left: "450px",
                top: "580px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r2"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title:
          'Marque a variável “a” para que guarde o valor 10 ao fim do algoritmo, depois, pressione, "Executar"',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Estrutura",
                styles: { fontSize: "30px" },
                src: "",
                width: "205px",
                height: "50px",
                left: "25px",
                top: "70px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variável  :",
                styles: { fontSize: "30px" },
                src: "",
                width: "224px",
                height: "50px",
                left: "55px",
                top: "135px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a : inteiro",
                styles: { fontSize: "30px" },
                src: "",
                width: "218px",
                height: "50px",
                left: "105px",
                top: "190px"
              },
              {
                id: "label3",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a = 10",
                styles: { fontSize: "30px" },
                src: "",
                width: "217px",
                height: "57px",
                left: "105px",
                top: "250px"
              },
              {
                id: "label4",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Inicio",
                styles: { fontSize: "30px" },
                src: "",
                width: "219px",
                height: "57px",
                left: "105px",
                top: "320px"
              },
              {
                id: "label5",
                type: "label",
                tabIndex: "",
                title: "",
                value: "x = 10",
                styles: { fontSize: "30px" },
                src: "",
                width: "193px",
                height: "52px",
                left: "145px",
                top: "385px"
              },
              {
                id: "label6",
                type: "label",
                tabIndex: "",
                title: "",
                value: "a + = 5",
                styles: { fontSize: "30px" },
                src: "",
                width: "185px",
                height: "58px",
                left: "165px",
                top: "505px"
              },
              {
                id: "label7",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim",
                styles: { fontSize: "30px" },
                src: "",
                width: "238px",
                height: "65px",
                left: "50px",
                top: "580px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "Escolha a variável corretamente.",
                value: "",
                styles: { fontSize: "23px" },
                src: "",
                width: "365px",
                height: "53px",
                left: "105px",
                top: "445px",
                values: {
                  r1: "Se “a” <= 10, então, a=2, fim_se.",
                  r2: "Se a==10, então, a<=5, fim_se.",
                  r3: "Se a>10, então, a<=5, fim_se."
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Executar",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "253px",
                height: "50px",
                left: "450px",
                top: "580px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r2"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      "Simulador 04: Selecione as frases corretas para formar a estrutura de decisão que representa a seguinte frase: “Caso a nota do aluno seja maior ou igual a 7, o aluno estará Aprovado. Caso contrário, ele estará Reprovado”.",
    items: [
      {
        title:
          "Selecione as frases em cada caixa, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "102px",
                height: "50px",
                left: "35px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "241px",
                height: "50px",
                left: "150px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select2",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "143px",
                height: "50px",
                left: "400px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select3",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "319px",
                height: "50px",
                left: "160px",
                top: "135px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select4",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "147px",
                height: "50px",
                left: "40px",
                top: "205px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select5",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "313px",
                height: "50px",
                left: "145px",
                top: "270px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select6",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "233px",
                height: "50px",
                left: "40px",
                top: "355px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Executar",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "276px",
                height: "50px",
                left: "305px",
                top: "490px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r7",
                        select1: "r1",
                        select2: "r2",
                        select3: "r3",
                        select4: "r4",
                        select5: "r5",
                        select6: "r6"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title:
          "Selecione as frases em cada caixa, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "Escolha a primeira variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "102px",
                height: "50px",
                left: "35px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "Escolha a segunda variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "241px",
                height: "50px",
                left: "150px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select2",
                type: "select",
                tabIndex: "",
                title: "Escolha a terceira variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "143px",
                height: "50px",
                left: "400px",
                top: "65px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select3",
                type: "select",
                tabIndex: "",
                title: "Escolha a quarta variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "319px",
                height: "50px",
                left: "160px",
                top: "135px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select4",
                type: "select",
                tabIndex: "",
                title: "Escolha a quinta variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "147px",
                height: "50px",
                left: "40px",
                top: "205px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select5",
                type: "select",
                tabIndex: "",
                title: "Escolha a sexta variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "313px",
                height: "50px",
                left: "145px",
                top: "270px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "select6",
                type: "select",
                tabIndex: "",
                title: "Escolha a sétma variável.",
                value: "",
                styles: { fontSize: "20px" },
                src: "",
                width: "233px",
                height: "50px",
                left: "40px",
                top: "355px",
                values: {
                  r1: "média >= 7",
                  r2: "Então",
                  r3: "Aluno Aprovado",
                  r4: "Senão",
                  r5: "Aluno Reprovado",
                  r6: "fim_se",
                  r7: "Se"
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Executar",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "276px",
                height: "50px",
                left: "305px",
                top: "490px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select0: "r7",
                        select1: "r1",
                        select2: "r2",
                        select3: "r3",
                        select4: "r4",
                        select5: "r5",
                        select6: "r6"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      "Simulador 5: Analise o algoritmo a seguir, depois,  complete as caixas a seguir, de forma que “Hora” e “Valor” atendam as especificações do algoritmo. ",
    items: [
      {
        title: "Selecione corretamente, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "text0",
                type: "textarea",
                tabIndex: "",
                title: "",
                value: `
  Variáveis 
hora: Inteiro
valor: Decimal 
início 
se hora >= 6 e hora <= 22 então 
    se valor <= 5000 então 
  Saque realizado com sucesso!
senão 
    valor não ser sacado!
fim_se 
Senão 
Se valor <= 400 então 
    Saque realizado com sucesso!
senão 
    Valor não ser sacado!
fim_se 
fim_se
FIM`,
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "23px",
                  backgroundColor: "transparent"
                },
                readOnly: true,
                src: "",
                width: "691px",
                height: "524px",
                left: "0px",
                top: "5px"
              },
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Hora",
                styles: { fontSize: "30px" },
                src: "",
                width: "138px",
                height: "50px",
                left: "90px",
                top: "550px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Valor",
                styles: { fontSize: "30px" },
                src: "",
                width: "140px",
                height: "50px",
                left: "390px",
                top: "555px"
              },
              {
                id: "cx_text0",
                type: "input",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "96px",
                height: "36px",
                left: "180px",
                top: "550px"
              },
              {
                id: "cx_text1",
                type: "input",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "97px",
                height: "39px",
                left: "480px",
                top: "550px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "194px",
                height: "50px",
                left: "280px",
                top: "625px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        cx_text0: ["!=", ""],
                        cx_text1: ["!=", ""]
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title: "Selecione corretamente, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "text0",
                type: "textarea",
                tabIndex: "",
                title: "",
                value: `
  Variáveis 
hora: Inteiro
valor: Decimal 
início 
se hora >= 6 e hora <= 22 então 
    se valor <= 5000 então 
  Saque realizado com sucesso!
senão 
    valor não ser sacado!
fim_se 
Senão 
Se valor <= 400 então 
    Saque realizado com sucesso!
senão 
    Valor não ser sacado!
fim_se 
fim_se
FIM`,
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "23px",
                  backgroundColor: "transparent"
                },
                readOnly: true,
                src: "",
                width: "691px",
                height: "524px",
                left: "0px",
                top: "5px"
              },
              {
                id: "label0",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "Hora",
                styles: { fontSize: "30px" },
                src: "",
                width: "138px",
                height: "50px",
                left: "90px",
                top: "550px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "Valor",
                styles: { fontSize: "30px" },
                src: "",
                width: "140px",
                height: "50px",
                left: "390px",
                top: "555px"
              },
              {
                id: "cx_text0",
                type: "input",
                tabIndex: "",
                title: "Hora.",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "96px",
                height: "36px",
                left: "180px",
                top: "550px"
              },
              {
                id: "cx_text1",
                type: "input",
                tabIndex: "",
                title: "Valor.",
                value: "",
                styles: { fontSize: "22px" },
                src: "",
                width: "97px",
                height: "39px",
                left: "480px",
                top: "550px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "Executar",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "194px",
                height: "50px",
                left: "280px",
                top: "625px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        cx_text0: ["!=", ""],
                        cx_text1: ["!=", ""]
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title:
      'Simulador 6: Selecione o estado do cliente de forma que o valor do imposto seja "0,4".',
    items: [
      {
        title: 'Selecione corretamente, depois, pressione, "Executar"',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "precoProduto:",
                styles: { fontSize: "25px" },
                src: "",
                width: "136px",
                height: "50px",
                left: "60px",
                top: "560px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "estadoCliente:",
                styles: { fontSize: "25px" },
                src: "",
                width: "139px",
                height: "50px",
                left: "320px",
                top: "560px"
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "25px" },
                src: "",
                width: "140px",
                height: "31px",
                left: "500px",
                top: "560px",
                values: {
                  r1: "caso 1",
                  r2: "caso 2",
                  r3: "caso 3"
                }
              },
              {
                id: "text1",
                type: "textarea",
                tabIndex: "",
                title: "",
                value: "",
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "23px",
                  backgroundColor: "transparent"
                },
                src: "",
                readOnly: true,
                value: `
  Variaveis 
precoProduto, valorImposto: Decimal 
estadoCliente: Inteiro 
Início
precoProduto = 10 
estadoCliente = 2
valorImposto = 0
selecione estadoCliente
caso 1
   valorImposto = 0,2
caso 2
   valor imposto = 0,4
caso 3
   valorImposto = 0,3
fim_selecione
vlaorImposto += 1
precoProduto = precoProduto * valorImposto
FIM`,
                width: "665px",
                height: "554px",
                left: "0px",
                top: "0px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "12",
                styles: { fontSize: "25px" },
                src: "",
                width: "72px",
                height: "43px",
                left: "225px",
                top: "560px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "205px",
                height: "50px",
                left: "280px",
                top: "620px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select1: "r2"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title: 'Selecione corretamente, depois, pressione, "Executar"',
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "text1",
                type: "textarea",
                tabIndex: "",
                title: "",
                styles: {
                  resize: "none",
                  border: "none",
                  fontSize: "23px",
                  backgroundColor: "transparent"
                },
                src: "",
                readOnly: true,
                value: `
  Variaveis 
precoProduto, valorImposto: Decimal 
estadoCliente: Inteiro 
Início
precoProduto = 10 
estadoCliente = 2
valorImposto = 0
selecione estadoCliente
caso 1
   valorImposto = 0,2
caso 2
   valor imposto = 0,4
caso 3
   valorImposto = 0,3
fim_selecione
vlaorImposto += 1
precoProduto = precoProduto * valorImposto
FIM`,
                width: "665px",
                height: "554px",
                left: "0px",
                top: "0px"
              },
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "precoProduto:",
                styles: { fontSize: "25px" },
                src: "",
                width: "136px",
                height: "50px",
                left: "60px",
                top: "560px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "12",
                styles: { fontSize: "25px" },
                src: "",
                width: "72px",
                height: "43px",
                left: "225px",
                top: "560px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "estadoCliente:",
                styles: { fontSize: "25px" },
                src: "",
                width: "139px",
                height: "50px",
                left: "320px",
                top: "560px"
              },
              {
                id: "select1",
                type: "select",
                tabIndex: "",
                title: "estadoCliente: Escolha uma opção.",
                value: "",
                styles: { fontSize: "25px" },
                src: "",
                width: "140px",
                height: "31px",
                left: "500px",
                top: "560px",
                values: {
                  r1: "caso 1",
                  r2: "caso 2",
                  r3: "caso 3"
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "205px",
                height: "50px",
                left: "280px",
                top: "620px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        select1: "r2"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "",
    title: "Simulador 7: Observe a estrutura dos algoritmos a seguir.",
    items: [
      {
        title:
          ": Escolha o algoritmo e a variável “tipoCliente” para que o desconto seja 2, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "checkbox0",
                type: "checkbox",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "" },
                src: "",
                width: "50px",
                height: "50px",
                left: "0px",
                top: "0px"
              },
              {
                id: "checkbox1",
                type: "checkbox",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "" },
                src: "",
                width: "50px",
                height: "50px",
                left: "395px",
                top: "0px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "",
                value: "",
                styles: { fontSize: "25px" },
                src: "",
                width: "247px",
                height: "50px",
                left: "420px",
                top: "495px",
                values: {
                  r1: "Caso 1",
                  r2: "Caso 2",
                  r3: "Caso 3",
                  r4: "Padrão"
                }
              },
              {
                id: "label0",
                type: "label",
                tabIndex: "",
                title: "",
                value: "tipoCliente:",
                styles: { fontSize: "30px" },
                src: "",
                width: "144px",
                height: "50px",
                left: "270px",
                top: "500px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variáveis ",
                styles: { fontSize: "22px" },
                src: "",
                width: "140px",
                height: "50px",
                left: "65px",
                top: "10px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "tipoCliente: Inteiro ",
                styles: { fontSize: "22px" },
                src: "",
                width: "186px",
                height: "52px",
                left: "65px",
                top: "35px"
              },
              {
                id: "label3",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto: Decimal ",
                styles: { fontSize: "22px" },
                src: "",
                width: "263px",
                height: "50px",
                left: "65px",
                top: "65px"
              },
              {
                id: "label4",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Início",
                styles: { fontSize: "22px" },
                src: "",
                width: "176px",
                height: "50px",
                left: "65px",
                top: "110px"
              },
              {
                id: "label5",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 0",
                styles: { fontSize: "22px" },
                src: "",
                width: "178px",
                height: "50px",
                left: "65px",
                top: "135px"
              },
              {
                id: "label6",
                type: "label",
                tabIndex: "",
                title: "",
                value: "selecione tipoCliente ",
                styles: { fontSize: "22px" },
                src: "",
                width: "242px",
                height: "50px",
                left: "75px",
                top: "160px"
              },
              {
                id: "label7",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 1",
                styles: { fontSize: "22px" },
                src: "",
                width: "238px",
                height: "50px",
                left: "90px",
                top: "185px"
              },
              {
                id: "label8",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 10",
                styles: { fontSize: "22px" },
                src: "",
                width: "198px",
                height: "50px",
                left: "110px",
                top: "210px"
              },
              {
                id: "label9",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "130px",
                height: "50px",
                left: "90px",
                top: "240px"
              },
              {
                id: "label10",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 5",
                styles: { fontSize: "22px" },
                src: "",
                width: "183px",
                height: "50px",
                left: "110px",
                top: "265px"
              },
              {
                id: "label11",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim_selecione",
                styles: { fontSize: "22px" },
                src: "",
                width: "190px",
                height: "50px",
                left: "100px",
                top: "300px"
              },
              {
                id: "label12",
                type: "label",
                tabIndex: "",
                title: "",
                value: "FIM",
                styles: { fontSize: "22px" },
                src: "",
                width: "162px",
                height: "50px",
                left: "65px",
                top: "335px"
              },
              {
                id: "label13",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variáveis ",
                styles: { fontSize: "22px" },
                src: "",
                width: "159px",
                height: "43px",
                left: "465px",
                top: "10px"
              },
              {
                id: "label14",
                type: "label",
                tabIndex: "",
                title: "",
                value: "tipoCliente: Inteiro",
                styles: { fontSize: "22px" },
                src: "",
                width: "273px",
                height: "50px",
                left: "465px",
                top: "35px"
              },
              {
                id: "label15",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto: Decimal",
                styles: { fontSize: "22px" },
                src: "",
                width: "249px",
                height: "50px",
                left: "465px",
                top: "65px"
              },
              {
                id: "label16",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Início",
                styles: { fontSize: "22px" },
                src: "",
                width: "270px",
                height: "50px",
                left: "465px",
                top: "110px"
              },
              {
                id: "label17",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 0",
                styles: { fontSize: "22px" },
                src: "",
                width: "249px",
                height: "50px",
                left: "465px",
                top: "135px"
              },
              {
                id: "label18",
                type: "label",
                tabIndex: "",
                title: "",
                value: "selecione tipoCliente",
                styles: { fontSize: "22px" },
                src: "",
                width: "262px",
                height: "50px",
                left: "480px",
                top: "160px"
              },
              {
                id: "label19",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 1",
                styles: { fontSize: "22px" },
                src: "",
                width: "261px",
                height: "50px",
                left: "495px",
                top: "185px"
              },
              {
                id: "label20",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 10",
                styles: { fontSize: "22px" },
                src: "",
                width: "244px",
                height: "51px",
                left: "510px",
                top: "215px"
              },
              {
                id: "label21",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "255px",
                height: "49px",
                left: "495px",
                top: "240px"
              },
              {
                id: "label22",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 5",
                styles: { fontSize: "22px" },
                src: "",
                width: "247px",
                height: "50px",
                left: "520px",
                top: "270px"
              },
              {
                id: "label23",
                type: "label",
                tabIndex: "",
                title: "",
                value: "padrão",
                styles: { fontSize: "22px" },
                src: "",
                width: "250px",
                height: "50px",
                left: "500px",
                top: "300px"
              },
              {
                id: "label24",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "241px",
                height: "50px",
                left: "525px",
                top: "330px"
              },
              {
                id: "label25",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim_selecione",
                styles: { fontSize: "22px" },
                src: "",
                width: "275px",
                height: "50px",
                left: "500px",
                top: "360px"
              },
              {
                id: "label26",
                type: "label",
                tabIndex: "",
                title: "",
                value: "FIM",
                styles: { fontSize: "22px" },
                src: "",
                width: "158px",
                height: "50px",
                left: "480px",
                top: "390px"
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "233px",
                height: "50px",
                left: "335px",
                top: "585px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        checkbox0: false,
                        checkbox1: true,
                        select0: "r4"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ],
    itemsBlind: [
      {
        title:
          ": Escolha o algoritmo e a variável “tipoCliente” para que o desconto seja 2, depois, pressione “Executar”.",
        done: false,
        video: "",
        states: [
          {
            done: false,
            components: [
              {
                id: "checkbox0",
                type: "checkbox",
                tabIndex: "",
                title: "Escolher algoritmo 1.",
                value: "",
                styles: { fontSize: "" },
                src: "",
                width: "50px",
                height: "50px",
                left: "0px",
                top: "0px"
              },
              {
                id: "label1",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variáveis ",
                styles: { fontSize: "22px" },
                src: "",
                width: "140px",
                height: "50px",
                left: "65px",
                top: "10px"
              },
              {
                id: "label2",
                type: "label",
                tabIndex: "",
                title: "",
                value: "tipoCliente: Inteiro ",
                styles: { fontSize: "22px" },
                src: "",
                width: "186px",
                height: "52px",
                left: "65px",
                top: "35px"
              },
              {
                id: "label3",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto: Decimal ",
                styles: { fontSize: "22px" },
                src: "",
                width: "263px",
                height: "50px",
                left: "65px",
                top: "65px"
              },
              {
                id: "label4",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Início",
                styles: { fontSize: "22px" },
                src: "",
                width: "176px",
                height: "50px",
                left: "65px",
                top: "110px"
              },
              {
                id: "label5",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 0",
                styles: { fontSize: "22px" },
                src: "",
                width: "178px",
                height: "50px",
                left: "65px",
                top: "135px"
              },
              {
                id: "label6",
                type: "label",
                tabIndex: "",
                title: "",
                value: "selecione tipoCliente ",
                styles: { fontSize: "22px" },
                src: "",
                width: "242px",
                height: "50px",
                left: "75px",
                top: "160px"
              },
              {
                id: "label7",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 1",
                styles: { fontSize: "22px" },
                src: "",
                width: "238px",
                height: "50px",
                left: "90px",
                top: "185px"
              },
              {
                id: "label8",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 10",
                styles: { fontSize: "22px" },
                src: "",
                width: "198px",
                height: "50px",
                left: "110px",
                top: "210px"
              },
              {
                id: "label9",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "130px",
                height: "50px",
                left: "90px",
                top: "240px"
              },
              {
                id: "label10",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 5",
                styles: { fontSize: "22px" },
                src: "",
                width: "183px",
                height: "50px",
                left: "110px",
                top: "265px"
              },
              {
                id: "label11",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim_selecione",
                styles: { fontSize: "22px" },
                src: "",
                width: "190px",
                height: "50px",
                left: "100px",
                top: "300px"
              },
              {
                id: "label12",
                type: "label",
                tabIndex: "",
                title: "",
                value: "FIM",
                styles: { fontSize: "22px" },
                src: "",
                width: "162px",
                height: "50px",
                left: "65px",
                top: "335px"
              },
              {
                id: "checkbox1",
                type: "checkbox",
                tabIndex: "",
                title: "Escolher algoritmo 2.",
                value: "",
                styles: { fontSize: "" },
                src: "",
                width: "50px",
                height: "50px",
                left: "395px",
                top: "0px"
              },
              {
                id: "label13",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Variáveis ",
                styles: { fontSize: "22px" },
                src: "",
                width: "159px",
                height: "43px",
                left: "465px",
                top: "10px"
              },
              {
                id: "label14",
                type: "label",
                tabIndex: "",
                title: "",
                value: "tipoCliente: Inteiro",
                styles: { fontSize: "22px" },
                src: "",
                width: "273px",
                height: "50px",
                left: "465px",
                top: "35px"
              },
              {
                id: "label15",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto: Decimal",
                styles: { fontSize: "22px" },
                src: "",
                width: "249px",
                height: "50px",
                left: "465px",
                top: "65px"
              },
              {
                id: "label16",
                type: "label",
                tabIndex: "",
                title: "",
                value: "Início",
                styles: { fontSize: "22px" },
                src: "",
                width: "270px",
                height: "50px",
                left: "465px",
                top: "110px"
              },
              {
                id: "label17",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 0",
                styles: { fontSize: "22px" },
                src: "",
                width: "249px",
                height: "50px",
                left: "465px",
                top: "135px"
              },
              {
                id: "label18",
                type: "label",
                tabIndex: "",
                title: "",
                value: "selecione tipoCliente",
                styles: { fontSize: "22px" },
                src: "",
                width: "262px",
                height: "50px",
                left: "480px",
                top: "160px"
              },
              {
                id: "label19",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 1",
                styles: { fontSize: "22px" },
                src: "",
                width: "261px",
                height: "50px",
                left: "495px",
                top: "185px"
              },
              {
                id: "label20",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 10",
                styles: { fontSize: "22px" },
                src: "",
                width: "244px",
                height: "51px",
                left: "510px",
                top: "215px"
              },
              {
                id: "label21",
                type: "label",
                tabIndex: "",
                title: "",
                value: "caso 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "255px",
                height: "49px",
                left: "495px",
                top: "240px"
              },
              {
                id: "label22",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 5",
                styles: { fontSize: "22px" },
                src: "",
                width: "247px",
                height: "50px",
                left: "520px",
                top: "270px"
              },
              {
                id: "label23",
                type: "label",
                tabIndex: "",
                title: "",
                value: "padrão",
                styles: { fontSize: "22px" },
                src: "",
                width: "250px",
                height: "50px",
                left: "500px",
                top: "300px"
              },
              {
                id: "label24",
                type: "label",
                tabIndex: "",
                title: "",
                value: "valorDesconto = 2",
                styles: { fontSize: "22px" },
                src: "",
                width: "241px",
                height: "50px",
                left: "525px",
                top: "330px"
              },
              {
                id: "label25",
                type: "label",
                tabIndex: "",
                title: "",
                value: "fim_selecione",
                styles: { fontSize: "22px" },
                src: "",
                width: "275px",
                height: "50px",
                left: "500px",
                top: "360px"
              },
              {
                id: "label26",
                type: "label",
                tabIndex: "",
                title: "",
                value: "FIM",
                styles: { fontSize: "22px" },
                src: "",
                width: "158px",
                height: "50px",
                left: "480px",
                top: "390px"
              },
              {
                id: "label0",
                type: "label",
                tabIndex: -1,
                title: "",
                value: "tipoCliente:",
                styles: { fontSize: "30px" },
                src: "",
                width: "144px",
                height: "50px",
                left: "270px",
                top: "500px"
              },
              {
                id: "select0",
                type: "select",
                tabIndex: "",
                title: "Escolha a variável tipoCliente corretamente.",
                value: "",
                styles: { fontSize: "25px" },
                src: "",
                width: "247px",
                height: "50px",
                left: "420px",
                top: "495px",
                values: {
                  r1: "Caso 1",
                  r2: "Caso 2",
                  r3: "Caso 3",
                  r4: "Padrão"
                }
              },
              {
                id: "button0",
                type: "button",
                tabIndex: "",
                title: "",
                value: "Executar",
                styles: { fontSize: "22px" },
                src: "",
                width: "233px",
                height: "50px",
                left: "335px",
                top: "585px",
                on: {
                  click: {
                    advanceState: true,
                    conditions: [
                      {
                        checkbox0: false,
                        checkbox1: true,
                        select0: "r4"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    done: false,
    video: "",
    defaultImage: "images_conteudo/iconeIntroducao.png",
    title:
      "Conclusão: Nesta aula, conhecemos as estruturas de decisão e aprendemos como elas funcionam. Além disso, pudemos entender que estruturas de decisão fazem parte do nosso cotidiano e que elas são muito importantes na elaboração de algoritmos. Mas não pare por aqui! Continue estudando! E, caso tenha dúvidas, converse com o seu tutor. Até mais!",
    items: [],
    itemsBlind: []
  }
];
