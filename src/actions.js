import * as R from "ramda";

import Page from "./models/Page";
import Item from "./models/Item";

import { ADD, REMOVE, CLONE, SET_ATTR, SET_SELECTED } from "./constants";

export const add = (path, el, type) => ({
  type: ADD,
  payload: {
    el,
    path,
    type
  }
});

export const remove = path => ({
  type: REMOVE,
  payload: path
});

export const clone = (path, index, el) => ({
  type: CLONE,
  payload: {
    el,
    path,
    index
  }
});

export const setAttr = (path, attr, value) => {
  const p = R.append(attr, path);

  return {
    type: SET_ATTR,
    payload: {
      value,
      path: p
    }
  };
};

export const setSelected = (path, type) => ({
  type: SET_SELECTED,
  payload: {
    path,
    type
  }
});
