import React from "react";

import { connect } from "react-redux";

import { ITEM, PAGE, STATE, COMPONENT } from "../constants";

import Drawer from "material-ui/Drawer";
import Divider from "material-ui/Divider";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";

import PageProperties from "./PageProperties";
import ItemProperties from "./ItemProperties";
import StateProperties from "./StateProperties";
import ComponentProperties from "./ComponentProperties";
import ComponentsList from "./ComponentsList";

const RightPane = ({ typeSelected }) => (
  <Drawer
    open={true}
    openSecondary={true}
    containerStyle={{ height: "calc(100% - 57px)", top: 57 }}
    width="18%"
  >
    {typeSelected === PAGE && <PageProperties />}
    {typeSelected === ITEM && <ItemProperties />}
    {typeSelected === STATE && <StateProperties />}
    {typeSelected === STATE && <Divider />}
    {typeSelected === COMPONENT && <ComponentProperties />}
    {(typeSelected === STATE || typeSelected === COMPONENT) && (
      <ComponentsList />
    )}
    {!typeSelected && (
      <div
        style={{
          height: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "#aaa"
        }}
      >
        Nenhum item selectionado
      </div>
    )}
  </Drawer>
);

export default connect(state => ({
  typeSelected: state.typeSelected,
  selectedItem: state.selectedItem
}))(RightPane);
