import React from "react";

import { connect } from "react-redux";

import * as R from "ramda";

import { remove, add, clone, setAttr } from "../actions";

import State from "../models/State";

import FontIcon from "material-ui/FontIcon";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";

import { TextField } from "../styles";

const ItemProperties = ({ profile, selectedPath, item, dispatch }) => (
  <div>
    <Subheader>Item {R.last(selectedPath) + 1}</Subheader>
    <List>
      <div style={{ paddingRight: 16, paddingLeft: 16 }}>
        <TextField
          multiLine={true}
          value={item.title}
          hintText="Enunciado"
          onChange={(e, newValue) =>
            dispatch(setAttr(selectedPath, "title", newValue))}
        />
      </div>
      <ListItem
        primaryText="Adcionar Imagem"
        leftIcon={
          <FontIcon className="material-icons" children="collections" />
        }
      />
      <ListItem
        primaryText="Adcionar Estado"
        onClick={() =>
          dispatch(add(R.concat(selectedPath, ["states"]), new State()))}
        leftIcon={<FontIcon className="material-icons" children="add" />}
      />
      <ListItem
        primaryText="Clonar Item"
        onClick={() =>
          dispatch(
            clone(R.init(selectedPath), R.last(selectedPath), R.clone(item))
          )}
        leftIcon={
          <FontIcon className="material-icons" children="content_copy" />
        }
      />
      <ListItem
        primaryText="Remover Item"
        onClick={() => dispatch(remove(selectedPath))}
        leftIcon={<FontIcon className="material-icons" children="remove" />}
      />
    </List>
  </div>
);

export default connect(state => {
  const item = R.view(R.lensPath(state.selectedPath), state);
  return {
    item,
    profile: state.profile,
    selectedPath: state.selectedPath
  };
})(ItemProperties);
