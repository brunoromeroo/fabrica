import React from "react";
import { connect } from "react-redux";

import * as R from "ramda";

import { remove, add, clone, setAttr } from "../actions";

import FontIcon from "material-ui/FontIcon";
import { Tabs, Tab } from "material-ui/Tabs";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";
import { GridList, GridTile } from "material-ui/GridList";

import { TextField } from "../styles";

const ComponentProperties = ({ component, dispatch, selectedPath }) => (
  <Tabs>
    <Tab icon={<FontIcon className="material-icons" children="settings" />}>
      <List>
        <div style={{ paddingRight: 16, paddingLeft: 16 }}>
          <TextField
            value={component.id}
            floatingLabelText="id"
            floatingLabelFixed={true}
            hintText="Digite o id do componente"
            onChange={(e, newValue) =>
              dispatch(setAttr(selectedPath, "id", newValue))}
          />
        </div>
        <ListItem
          primaryText="Clonar Componente"
          onClick={() =>
            dispatch(
              clone(
                R.init(selectedPath),
                R.last(selectedPath),
                R.clone(component)
              )
            )}
          leftIcon={
            <FontIcon className="material-icons" children="content_copy" />
          }
        />
        <ListItem
          primaryText="Remover Componente"
          onClick={() => dispatch(remove(selectedPath))}
          leftIcon={<FontIcon className="material-icons" children="remove" />}
        />
      </List>
    </Tab>
    <Tab
      icon={
        <FontIcon className="material-icons" children="settings_input_svideo" />
      }
    >
      <div style={{ padding: 16 }}>
        <GridTile cols={2} rows={1}>
          <TextField
            floatingLabelText="value"
            value={component.value}
            floatingLabelFixed={true}
            hintText="Digite o value"
            onChange={(e, newValue) =>
              dispatch(setAttr(selectedPath, "value", newValue))}
          />
        </GridTile>
        <GridList cols={2} cellHeight="auto">
          {Object.keys(
            R.compose(
              R.dissoc("id"),
              R.dissoc("type"),
              R.dissoc("label"),
              R.dissoc("styles"),
              R.dissoc("value"),
              R.ifElse(
                R.always(component.type === "img"),
                R.dissoc("value"),
                R.dissoc("src")
              )
            )(component)
          ).map(i => (
            <GridTile cols={2} rows={1} key={i}>
              <div
                style={{
                  width: "48%",
                  marginRight: "2%",
                  display: "inline-block"
                }}
              >
                <TextField
                  value={i}
                  floatingLabelText="Chave"
                  floatingLabelFixed={true}
                  hintText="Digite a chave"
                  onBlur={(e, newValue) =>
                    dispatch(setAttr(selectedPath, newValue, ""))}
                />
              </div>
              <div style={{ width: "48%", display: "inline-block" }}>
                <TextField
                  value={component[i]}
                  floatingLabelText="Valor"
                  floatingLabelFixed={true}
                  hintText="Digite o valor"
                  onChange={(e, newValue) =>
                    dispatch(setAttr(selectedPath, i, newValue))}
                />
              </div>
            </GridTile>
          ))}
          <GridTile cols={2} rows={1}>
            <FlatButton
              primary
              fullWidth={true}
              label="Nova Própriedade"
              onClick={() => dispatch(setAttr(selectedPath, "", ""))}
              icon={<FontIcon className="material-icons" children="add" />}
            />
          </GridTile>
        </GridList>
      </div>
    </Tab>
    <Tab icon={<FontIcon className="material-icons" children="style" />}>
      <List>
        <div style={{ paddingRight: 16, paddingLeft: 16 }}>
          <TextField
            value={component.id}
            floatingLabelText="id"
            floatingLabelFixed={true}
            hintText="Digite o id do componente"
            onChange={(e, newValue) =>
              dispatch(setAttr(selectedPath, "id", newValue))}
          />
        </div>
        <ListItem
          primaryText="Clonar Componente"
          onClick={() =>
            dispatch(
              clone(
                R.init(selectedPath),
                R.last(selectedPath),
                R.clone(component)
              )
            )}
          leftIcon={
            <FontIcon className="material-icons" children="content_copy" />
          }
        />
        <ListItem
          primaryText="Remover Componente"
          onClick={() => dispatch(remove(selectedPath))}
          leftIcon={<FontIcon className="material-icons" children="remove" />}
        />
      </List>
    </Tab>
    <Tab icon={<FontIcon className="material-icons" children="event" />}>
      <List>
        <div style={{ paddingRight: 16, paddingLeft: 16 }}>
          <TextField
            value={component.id}
            floatingLabelText="id"
            floatingLabelFixed={true}
            hintText="Digite o id do componente"
            onChange={(e, newValue) =>
              dispatch(setAttr(selectedPath, "id", newValue))}
          />
        </div>
        <ListItem
          primaryText="Clonar Componente"
          onClick={() =>
            dispatch(
              clone(
                R.init(selectedPath),
                R.last(selectedPath),
                R.clone(component)
              )
            )}
          leftIcon={
            <FontIcon className="material-icons" children="content_copy" />
          }
        />
        <ListItem
          primaryText="Remover Componente"
          onClick={() => dispatch(remove(selectedPath))}
          leftIcon={<FontIcon className="material-icons" children="remove" />}
        />
      </List>
    </Tab>
  </Tabs>
);

export default connect(state => ({
  selectedPath: state.selectedPath,
  component: R.view(R.lensPath(state.selectedPath), state)
}))(ComponentProperties);
