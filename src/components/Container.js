import React from "react";

import Drawer from "material-ui/Drawer";
import { List, ListItem } from "material-ui/List";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";

import LeftPane from "./LeftPane";
import RightPane from "./RightPane";
import Scene from "./Scene";

export default () => (
  <div>
    <LeftPane
      pages={[
        {
          title: "Página 1"
        }
      ]}
    />
    <Scene />
    <RightPane />
  </div>
);
