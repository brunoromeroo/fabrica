import React from "react";
import * as R from "ramda";

import Page from "../models/Page";

import { connect } from "react-redux";
import { PAGE, ITEM, STATE } from "../constants";
import { addPage, setSelected, add } from "../actions";

import Drawer from "material-ui/Drawer";
import FontIcon from "material-ui/FontIcon";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";

const isOpen = (path, itemPath) =>
  R.equals(R.take(itemPath.length, path), itemPath);

const LeftPane = ({ template, profile, dispatch, selectedPath }) => (
  <Drawer
    open={true}
    containerStyle={{ height: "calc(100% - 57px)", top: 57 }}
    width="18%"
  >
    <List>
      <Subheader style={{ paddingRight: 16 }}>
        <FlatButton
          primary
          label="Nova Página"
          fullWidth={true}
          onClick={() => dispatch(add(["template"], new Page()))}
          icon={<FontIcon className="material-icons" children="add" />}
        />
      </Subheader>
      {template &&
        template.map((page, p) => (
          <ListItem
            key={p}
            open={isOpen(selectedPath, ["template", p])}
            style={{
              backgroundColor: R.equals(["template", p], selectedPath)
                ? "rgba(0, 0, 0, .1)"
                : "transparent"
            }}
            onClick={() => dispatch(setSelected(["template", p], PAGE))}
            primaryText={`Página ${p + 1}`}
            nestedItems={page[profile].map((item, i) => (
              <ListItem
                key={i}
                open={isOpen(selectedPath, ["template", p, profile, i])}
                style={{
                  backgroundColor: R.equals(
                    ["template", p, profile, i],
                    selectedPath
                  )
                    ? "rgba(0, 0, 0, .1)"
                    : "transparent"
                }}
                onClick={() =>
                  dispatch(setSelected(["template", p, profile, i], ITEM))}
                primaryText={`Item ${i + 1}`}
                nestedItems={item.states.map((state, s) => (
                  <ListItem
                    open={isOpen(selectedPath, [
                      "template",
                      p,
                      profile,
                      i,
                      "states",
                      s
                    ])}
                    key={s}
                    style={{
                      backgroundColor: R.equals(
                        ["template", p, profile, i, "states", s],
                        selectedPath
                      )
                        ? "rgba(0, 0, 0, .1)"
                        : "transparent"
                    }}
                    primaryText={`Estado ${s + 1}`}
                    style={{
                      backgroundColor: isOpen(selectedPath, [
                        "template",
                        p,
                        profile,
                        i,
                        "states",
                        s
                      ])
                        ? "rgba(0, 0, 0, .1)"
                        : "transparent"
                    }}
                    onClick={() =>
                      dispatch(
                        setSelected(
                          ["template", p, profile, i, "states", s],
                          STATE
                        )
                      )}
                  />
                ))}
              />
            ))}
          />
        ))}
    </List>
  </Drawer>
);

export default connect(state => ({
  profile: state.profile,
  template: state.template,
  selectedPath: state.selectedPath
}))(LeftPane);
