import React from "react";

import { connect } from "react-redux";

import * as R from "ramda";

import Input from "../models/Input";
import Label from "../models/Label";
import Select from "../models/Select";
import Button from "../models/Button";
import Checkbox from "../models/Checkbox";
import TextArea from "../models/TextArea";

import { remove, add } from "../actions";

import { COMPONENT } from "../constants";

import FontIcon from "material-ui/FontIcon";
import Subheader from "material-ui/Subheader";
import { List, ListItem } from "material-ui/List";

const curryiedAdd = R.curry(add);

const ComponentsList = ({ dispatch, addComponent }) => (
  <List>
    <Subheader>Adcionar componente</Subheader>
    <ListItem
      primaryText="Label"
      onClick={() => dispatch(addComponent(new Label()))}
      leftIcon={<FontIcon className="material-icons" children="label" />}
    />
    <ListItem
      primaryText="Imagem"
      onClick={() => dispatch(addComponent(new Label()))}
      leftIcon={<FontIcon className="material-icons" children="collections" />}
    />
    <ListItem
      primaryText="Input"
      onClick={() => dispatch(addComponent(new Input()))}
      leftIcon={<FontIcon className="material-icons" children="input" />}
    />
    <ListItem
      primaryText="TextArea"
      onClick={() => dispatch(addComponent(new TextArea()))}
      leftIcon={<FontIcon className="material-icons" children="text_fields" />}
    />
    <ListItem
      primaryText="Button"
      onClick={() => dispatch(addComponent(new Button()))}
      leftIcon={<FontIcon className="material-icons" children="add_box" />}
    />
    <ListItem
      primaryText="Select"
      onClick={() => dispatch(addComponent(new Select()))}
      leftIcon={
        <FontIcon
          className="material-icons"
          children="arrow_drop_down_circle"
        />
      }
    />
    <ListItem
      primaryText="Checkbox"
      onClick={() => dispatch(addComponent(new Checkbox()))}
      leftIcon={<FontIcon className="material-icons" children="check_box" />}
    />
  </List>
);

export default connect(state => ({
  addComponent: curryiedAdd(
    R.concat(state.lastStateSelectedPath, ["components"]),
    R.__,
    COMPONENT
  )
}))(ComponentsList);
