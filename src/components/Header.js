import React from "react";

import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator,
  ToolbarTitle
} from "material-ui/Toolbar";

import FontIcon from "material-ui/FontIcon";

import FlatButton from "material-ui/FlatButton";

export default () => (
  <Toolbar
    style={{
      zIndex: 1300,
      width: "100%",
      position: "fixed"
    }}
  >
    <ToolbarGroup firstChild={true}>
      <FlatButton
        primary
        label="Abrir"
        style={{ marginLeft: 5, marginRight: 0 }}
        icon={<FontIcon className="material-icons">folder</FontIcon>}
      />
      <FlatButton
        primary
        label="Salvar"
        style={{ marginLeft: 5, marginRight: 0 }}
        icon={<FontIcon className="material-icons">save</FontIcon>}
      />
      <FlatButton
        primary
        label="Visualizar"
        style={{ marginLeft: 5, marginRight: 0 }}
        icon={<FontIcon className="material-icons">remove_red_eye</FontIcon>}
      />
    </ToolbarGroup>
  </Toolbar>
);
