import React, { Component } from "react";
import Draggable from "react-draggable";

import { connect } from "react-redux";

import * as R from "ramda";

import { setSelected, setAttr } from "../actions";
import { COMPONENT } from "../constants";

class Instance extends Component {
  setPosition = (e, data) => {
    this.props.dispatch(setAttr(this.props.selectedPath, "left", data.x));
    this.props.dispatch(setAttr(this.props.selectedPath, "top", data.y));
  };

  styles = () => {
    const component = this.props.component;
    const type = component.type;
    const styles = component.styles ? component.styles : {};

    return {
      ...styles
    };
  };

  attrs = () => {
    const component = this.props.component;

    let display = "block";

    if (component.shown !== undefined && !component.shown) {
      display = "none";
    }

    const comp = R.clone(component);

    delete comp.on;
    delete comp.top;
    delete comp.left;
    delete comp.width;
    delete comp.value;
    delete comp.height;
    delete comp.styles;
    delete comp.isLabel;

    return {
      display,
      ...comp,
      readOnly: true,
      value: component.value,
      style: {
        ...this.styles(),
        resize: "none",
        position: "absolute",
        width: component.width,
        height: component.height
      }
    };
  };

  children = () => {
    const component = this.props.component;

    const values = component.values;
    const children = R.toPairs(values).map((value, i) => (
      <option key={i + 1} title={value[1]} value={value[0]}>
        {value[1]}
      </option>
    ));

    return R.prepend(
      <option key={0} value="" title="Escolha">
        Escolha
      </option>,
      children
    );
  };

  render() {
    const component = this.props.component;

    const type = component.type;
    let el = React.createElement(type, this.attrs());

    if (type === "select") {
      el = React.createElement(type, this.attrs(), this.children());
    } else if (type === "button" || type === "label" || type === "p") {
      el = React.createElement(type, this.attrs(), component.value);
    } else if (type === "checkbox") {
      el = React.createElement("input", {
        ...this.attrs(),
        type: "checkbox"
      });
    }

    return (
      <Draggable
        bounds="parent"
        position={{
          y: component.top,
          x: component.left
        }}
        onMouseDown={() =>
          this.props.dispatch(setSelected(this.props.path, COMPONENT))}
        onStop={this.setPosition}
      >
        {el}
      </Draggable>
    );
  }
}

export default connect(state => ({
  selectedPath: state.selectedPath
}))(Instance);
