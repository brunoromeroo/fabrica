import React from "react";

import * as R from "ramda";

import { connect } from "react-redux";

import { remove, clone } from "../actions";

import Item from "../models/Item";

import FontIcon from "material-ui/FontIcon";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";

const StateProperties = ({ dispatch, selectedPath, state }) => (
  <div>
    <Subheader>Estado {R.last(selectedPath) + 1}</Subheader>
    <List>
      <ListItem
        primaryText="Clonar Estado"
        onClick={() =>
          dispatch(
            clone(R.init(selectedPath), R.last(selectedPath), R.clone(state))
          )}
        leftIcon={
          <FontIcon className="material-icons" children="content_copy" />
        }
      />
      <ListItem
        primaryText="Remover Estado"
        onClick={() => dispatch(remove(selectedPath))}
        leftIcon={<FontIcon className="material-icons" children="remove" />}
      />
    </List>
  </div>
);

export default connect(state => {
  const s = R.view(R.lensPath(state.selectedPath), state);
  return {
    state: s,
    selectedPath: state.selectedPath
  };
})(StateProperties);
