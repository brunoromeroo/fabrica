import React from "react";
import * as R from "ramda";

import { connect } from "react-redux";

import { STATE, COMPONENT } from "../constants";

import Instance from "./Instance";
import Paper from "material-ui/Paper";

const getStatePath = path => R.take(6, path);

const Scene = ({ state, selectedPath, typeSelected }) => (
  <Paper
    zDepth={2}
    style={{
      top: 160,
      left: "29%",
      width: 800,
      height: 700,
      position: "absolute"
    }}
  >
    {(typeSelected === STATE || typeSelected === COMPONENT) &&
      state.components.map((component, i) => (
        <Instance
          key={i}
          component={component}
          path={R.concat(getStatePath(selectedPath), ["components", i])}
        />
      ))}
  </Paper>
);

export default connect(state => {
  const s = R.view(R.lensPath(getStatePath(state.selectedPath)), state);

  return {
    state: s,
    typeSelected: state.typeSelected,
    selectedPath: state.selectedPath
  };
})(Scene);
