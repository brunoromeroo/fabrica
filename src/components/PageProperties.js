import React from "react";

import { connect } from "react-redux";

import * as R from "ramda";

import { remove, add, clone, setAttr } from "../actions";

import Item from "../models/Item";

import FontIcon from "material-ui/FontIcon";
import Subheader from "material-ui/Subheader";
import FlatButton from "material-ui/FlatButton";
import { List, ListItem } from "material-ui/List";

import { TextField } from "../styles";

const PageProperties = ({ dispatch, profile, selectedPath, page }) => (
  <div>
    <Subheader>Página {R.last(selectedPath) + 1}</Subheader>
    <List>
      <div style={{ paddingRight: 16, paddingLeft: 16 }}>
        <TextField
          multiLine={true}
          value={page.title}
          hintText="Enunciado"
          onChange={(e, newValue) =>
            dispatch(setAttr(selectedPath, "title", newValue))}
        />
      </div>
      <ListItem
        primaryText="Adcionar Imagem"
        leftIcon={
          <FontIcon className="material-icons" children="collections" />
        }
      />
      <ListItem
        primaryText="Adcionar Item"
        onClick={() =>
          dispatch(add(R.append(profile, selectedPath), new Item()))}
        leftIcon={<FontIcon className="material-icons" children="add" />}
      />
      <ListItem
        primaryText="Clonar Página"
        onClick={() =>
          dispatch(
            clone(R.init(selectedPath), R.last(selectedPath), R.clone(page))
          )}
        leftIcon={
          <FontIcon className="material-icons" children="content_copy" />
        }
      />
      <ListItem
        primaryText="Remover Página"
        onClick={() => dispatch(remove(selectedPath))}
        leftIcon={<FontIcon className="material-icons" children="remove" />}
      />
    </List>
  </div>
);

export default connect(state => {
  const page = R.view(R.lensPath(state.selectedPath), state);
  return {
    page,
    profile: state.profile,
    selectedPath: state.selectedPath
  };
})(PageProperties);
