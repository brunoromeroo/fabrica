import * as R from "ramda";
import {
  ADD,
  ITEM,
  PAGE,
  CLONE,
  STATE,
  REMOVE,
  SET_ATTR,
  COMPONENT,
  SET_SELECTED
} from "./constants";

const initialState = {
  template: [],
  profile: "items",
  typeSelected: "",
  selectedPath: [],
  lastStateSelectedPath: ""
};

const setPath = state => path => type =>
  R.compose(
    R.set(R.lensProp("selectedPath"), path),
    R.set(R.lensProp("typeSelected"), type)
  )(state);

const remove = (state, path) => {
  const lastPath = R.last(path);
  const isNumber = R.is(Number);
  const elPath = R.lensPath(R.init(path));
  const newState = R.over(elPath, R.remove(lastPath, 1), state);

  if (lastPath === R.view(elPath, state).length - 1) {
    if (lastPath === 0) {
      const i = R.findLastIndex(isNumber, R.init(path));
      const statefull = setPath(newState);

      if (i === -1) return statefull([])("");

      const p = R.append(path[i], R.take(i, path));
      const numbers = R.filter(isNumber, p);

      const set = statefull(p);

      switch (numbers.length) {
        case 1:
          return set(PAGE);
        case 2:
          return set(ITEM);
        case 3:
          return set(STATE);
        case 4:
          return set(COMPONENT);
      }
    } else {
      const last = lastPath - 1;

      return R.set(
        R.lensProp("selectedPath"),
        R.append(last, R.init(path)),
        newState
      );
    }
  }

  return R.set(R.lensPath(elPath), R.remove(R.last(path), 1), newState);
};

const add = (state, path, el, setPath) =>
  R.over(R.lensPath(path), R.append(el), state);

const clone = (state, path, index, el) =>
  R.over(R.lensPath(path), R.insert(index, el), state);

const setAttr = (state, path, value) => R.set(R.lensPath(path), value, state);

const setSelected = (state, path, type) =>
  R.compose(
    R.set(R.lensProp("selectedPath"), path),
    R.set(R.lensProp("typeSelected"), type),
    R.ifElse(
      R.always(R.equals(type, STATE)),
      R.set(R.lensProp("lastStateSelectedPath"), path),
      R.identity
    )
  )(state);

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD:
      const newState = add(state, action.payload.path, action.payload.el);
      if (action.payload.type) {
        const newPath = R.append(
          R.view(R.lensPath(action.payload.path), newState).length - 1,
          action.payload.path
        );

        return setSelected(newState, newPath, action.payload.type);
      }

      return newState;

    case CLONE:
      return clone(
        state,
        action.payload.path,
        action.payload.index,
        action.payload.el,
        action.payload.setPath
      );

    case REMOVE:
      return remove(state, action.payload);

    case SET_ATTR:
      return setAttr(state, action.payload.path, action.payload.value);

    case SET_SELECTED:
      return setSelected(state, action.payload.path, action.payload.type);
  }

  return state;
};
