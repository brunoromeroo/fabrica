export const ITEM = "ITEM";
export const PAGE = "PAGE";
export const STATE = "STATE";
export const COMPONENT = "COMPONENT";

export const ADD = "ADD";
export const CLONE = "CLONE";
export const REMOVE = "REMOVE";
export const SET_ATTR = "SET_ATTR";
export const SET_SELECTED = "SET_SELECTED";
