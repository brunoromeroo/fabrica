import React from "react";

import TF from "material-ui/TextField";

export const TextField = ({ style, ...props }) => (
  <TF
    style={{
      ...style,
      ...{
        width: "100%",
        maxWidth: "100%"
      }
    }}
    {...props}
  />
);
